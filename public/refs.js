console.log(list);
console.log(srcs);

var results = [];
var remainingRefs = list.length;
setRemainingRefs(remainingRefs);
list.reduce((p, ref) => {
    return p.then(() => {
        let search = {...ref, ...srcs };
        return postData("/getref", search).then((data) => {
                console.log('Success:', data);
                results.push(data);
                if (search.inputBsl) {
                    fillHtmlBSL(data);
                }
                if (search.inputDsl) {
                    fillHtmlDSL(data);
                }
                if (search.inputWebsite) {
                    fillHtmlWebsite(data);
                }
                if (search.inputGo2se) {
                    fillHtmlGo2SE(data);
                }
                if (search.inputSr) {
                    fillHtmlSR(data);
                }
                remainingRefs--;
                if (remainingRefs > 0) {
                    setRemainingRefs(remainingRefs);
                } else {
                    endCheck(results);
                }
                return true;
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    });
}, Promise.resolve());

async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(data)
    });
    return response.json();
}

function fillHtmlBSL(data) {
    if (data.product && data.product.parentRangeId) {
        document.getElementById("rge_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("rge_" + data.reference + "_" + data.locale).children[0].title = data.product.parentRangeId;
    } else {
        document.getElementById("rge_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }

    if (data.product && data.product.attributes.globalStatus == "Commercialised") {
        document.getElementById("stat_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("stat_" + data.reference + "_" + data.locale).children[0].title = data.product.attributes.globalStatus;
    } else if (data.product.attributes.globalStatus) {
        document.getElementById("stat_" + data.reference + "_" + data.locale).children[0].classList.add("is-warning");
        document.getElementById("stat_" + data.reference + "_" + data.locale).children[0].title = data.product.attributes.globalStatus;
    } else {
        document.getElementById("stat_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }

    if (data.product && data.product.longDescription) {
        document.getElementById("ld_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("ld_" + data.reference + "_" + data.locale).children[0].title = data.product.longDescription;
    } else {
        document.getElementById("ld_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");

    }

    if (data.product && data.product.shortDescription) {
        document.getElementById("sd_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("sd_" + data.reference + "_" + data.locale).children[0].title = data.product.shortDescription;
    } else {
        document.getElementById("sd_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }



    if (data.bslStatus && data.bslStatus.hasImage) {
        document.getElementById("img_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("img_" + data.reference + "_" + data.locale).children[0].title = data.bslStatus.hasImage;
    } else {
        document.getElementById("img_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }

    if (data.bslStatus && data.bslStatus.hasDocuments.length > 0) {
        document.getElementById("docs_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("docs_" + data.reference + "_" + data.locale).children[0].title = data.bslStatus.hasDocuments.length;
    } else {
        document.getElementById("docs_" + data.reference + "_" + data.locale).children[0].classList.add("is-warning");
        document.getElementById("docs_" + data.reference + "_" + data.locale).children[0].title = data.bslStatus.hasDocuments.length;
    }
}

function fillHtmlDSL(data) {

    if (data.dslStatus.hasDescriptionDSL) {
        document.getElementById("dslDesc_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("dslDesc_" + data.reference + "_" + data.locale).children[0].title = data.dslStatus.hasDescriptionDSL;
    } else {
        document.getElementById("dslDesc_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }

    if (data.dslStatus.hasImage) {
        document.getElementById("dslImg_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("dslImg_" + data.reference + "_" + data.locale).children[0].title = data.dslStatus.hasImage;
    } else {
        document.getElementById("dslImg_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }
}

function fillHtmlWebsite(data) {
    if (data.availableInWebsite) {
        document.getElementById("datash_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("datash_" + data.reference + "_" + data.locale).children[0].title = data.availableInWebsite;
    } else {
        document.getElementById("datash_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }
}

function fillHtmlGo2SE(data) {
    if (data.hasGo2SE.isWorking) {
        document.getElementById("go2SE_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("go2SE_" + data.reference + "_" + data.locale).children[0].title = data.hasGo2SE.isWorking;
    } else {
        document.getElementById("go2SE_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }
    if (data.hasGo2SE.hasDescription) {
        document.getElementById("go2SEDesc_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("go2SEDesc_" + data.reference + "_" + data.locale).children[0].title = data.hasGo2SE.hasDescription;
    } else {
        document.getElementById("go2SEDesc_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }
    if (data.hasGo2SE.hasImage) {
        document.getElementById("go2SEImg_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("go2SEImg_" + data.reference + "_" + data.locale).children[0].title = data.hasGo2SE.hasImage;
    } else {
        document.getElementById("go2SEImg_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }
    if (data.hasGo2SE.hasCharacteristics) {
        document.getElementById("go2SEChar_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("go2SEChar_" + data.reference + "_" + data.locale).children[0].title = data.hasGo2SE.hasCharacteristics;
    } else {
        document.getElementById("go2SEChar_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }
}

function fillHtmlSR(data) {
    if (data.hasSafeRepo.isWorking) {
        document.getElementById("sr_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("sr_" + data.reference + "_" + data.locale).children[0].title = data.hasSafeRepo.isWorking;
    } else {
        document.getElementById("sr_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }
    if (data.hasSafeRepo.hasDescription) {
        document.getElementById("srDesc_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("srDesc_" + data.reference + "_" + data.locale).children[0].title = data.hasSafeRepo.hasDescription;
    } else {
        document.getElementById("srDesc_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }
    if (data.hasSafeRepo.hasImage) {
        document.getElementById("srImg_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("srImg_" + data.reference + "_" + data.locale).children[0].title = data.hasSafeRepo.hasImage;
    } else {
        document.getElementById("srImg_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }
    if (data.hasSafeRepo.hasDocuments) {
        document.getElementById("srDocs_" + data.reference + "_" + data.locale).children[0].classList.add("is-success");
        document.getElementById("srDocs_" + data.reference + "_" + data.locale).children[0].title = data.hasSafeRepo.hasDocuments;
    } else {
        document.getElementById("srDocs_" + data.reference + "_" + data.locale).children[0].classList.add("is-danger");
    }
}

function endCheck() {
    document.getElementsByClassName("remaining-refs")[0].style.display = "none";
    document.getElementsByClassName("all-refs-checked")[0].querySelector("a").href = "data:application/json;charset=utf-8," + encodeURIComponent(JSON.stringify(results));
    document.getElementsByClassName("all-refs-checked")[0].style.display = "block";
}

function setRemainingRefs(remainingRefs) {
    document.getElementById("nbRemaining").innerHTML = remainingRefs + " references (less than " + Math.floor(remainingRefs * 20 / 60) + 1 + "min)";
}