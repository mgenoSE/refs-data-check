var scopelist = [
    { "pim" : true,
        "country":"gb",
        "language":"en",
        "name":"Global"
     },
     { "pim" : false,
       "country":"ww",
       "language":"en",
       "name":"Global"
    },
    { "pim" : false,
       "country":"ww",
       "language":"fr",
       "name":"Global (French)"
    },
    { "pim" : false,
       "country":"br",
       "language":"pt",
       "name":"Brazil"
    },
    { "pim" : false,
       "country":"ca",
       "language":"en",
       "name":"Canada"
    },
    { "pim" : true,
       "country":"cn",
       "language":"zh",
       "name":"China"
    },
    { "pim" : false,
       "country":"dk",
       "language":"da",
       "name":"Denmark"
    },
    { "pim" : false,
       "country":"fr",
       "language":"fr",
       "name":"France"
    },
    { "pim" : false,
       "country":"de",
       "language":"de",
       "name":"Germany"
    },
    { "pim" : true,
       "country":"in",
       "language":"en",
       "name":"India"
    },
    { "pim" : false,
       "country":"ru",
       "language":"ru",
       "name":"Russia"
    },
    { "pim" : false,
       "country":"us",
       "language":"en",
       "name":"USA"
    },
    { "pim" : true,
       "country":"uk",
       "language":"en",
       "name":"United Kingdom"
    },
    { "pim" : true,
       "country":"au",
       "language":"en",
       "name":"Australia"
    },
    { "pim" : false,
       "country":"bd",
       "language":"en",
       "name":"Bangladesh"
    },
    { "pim" : false,
       "country":"bn",
       "language":"en",
       "name":"Brunei"
    },
    { "pim" : false,
       "country":"kh",
       "language":"en",
       "name":"Cambodia"
    },
    { "pim" : false,
       "country":"cn",
       "language":"zh",
       "name":"China"
    },
    { "pim" : false,
       "country":"fj",
       "language":"en",
       "name":"Fiji, Tonga"
    },
    { "pim" : false,
       "country":"pf",
       "language":"fr",
       "name":"French Polynesia"
    },
    { "pim" : true,
       "country":"hk",
       "language":"en",
       "name":"Hong Kong, China"
    },
    { "pim" : true,
        "country":"hk",
        "language":"zh",
        "name":"Hong Kong, China"
     },
    { "pim" : false,
       "country":"in",
       "language":"en",
       "name":"India"
    },
    { "pim" : false,
       "country":"id",
       "language":"id",
       "name":"Indonesia (Bahasa)"
    },
    { "pim" : true,
       "country":"id",
       "language":"en",
       "name":"Indonesia (English)"
    },
    { "pim" : true,
       "country":"jp",
       "language":"ja",
       "name":"Japan"
    },
    { "pim" : true,
       "country":"kz",
       "language":"ru",
       "name":"Kazakhstan"
    },
    { "pim" : true,
       "country":"kr",
       "language":"ko",
       "name":"Korea, South"
    },
    { "pim" : false,
       "country":"la",
       "language":"en",
       "name":"Laos"
    },
    { "pim" : false,
       "country":"my",
       "language":"en",
       "name":"Malaysia"
    },
    { "pim" : false,
       "country":"mv",
       "language":"en",
       "name":"Maldives"
    },
    { "pim" : false,
       "country":"mn",
       "language":"mn",
       "name":"Mongolia"
    },
    { "pim" : false,
       "country":"mn",
       "language":"en",
       "name":"Mongolia (English)"
    },
    { "pim" : false,
       "country":"mm",
       "language":"my",
       "name":"Myanmar (Burmese)"
    },
    { "pim" : false,
       "country":"mm",
       "language":"en",
       "name":"Myanmar (English)"
    },
    { "pim" : false,
       "country":"nc",
       "language":"fr",
       "name":"New Caledonia"
    },
    { "pim" : true,
       "country":"nz",
       "language":"en",
       "name":"New Zealand"
    },
    { "pim" : false,
       "country":"pg",
       "language":"en",
       "name":"Papua New Guinea"
    },
    { "pim" : true,
       "country":"ph",
       "language":"en",
       "name":"Philippines"
    },
    { "pim" : true,
       "country":"sg",
       "language":"en",
       "name":"Singapore"
    },
    { "pim" : false,
       "country":"lk",
       "language":"en",
       "name":"Sri Lanka"
    },
    { "pim" : true,
       "country":"tw",
       "language":"zh",
       "name":"Taiwan, China"
    },
    { "pim" : true,
       "country":"th",
       "language":"en",
       "name":"Thailand (English)"
    },
    { "pim" : true,
       "country":"th",
       "language":"th",
       "name":"Thailand (Thai)"
    },
    { "pim" : false,
       "country":"uz",
       "language":"ru",
       "name":"Uzbekistan"
    },
    { "pim" : false,
       "country":"uz",
       "language":"en",
       "name":"Uzbekistan (English)"
    },
    { "pim" : true,
       "country":"vn",
       "language":"en",
       "name":"Vietnam"
    },
    { "pim" : true,
       "country":"vn",
       "language":"vi",
       "name":"Vietnam"
    },
    { "pim" : false,
       "country":"al",
       "language":"sq",
       "name":"Albania"
    },
    { "pim" : true,
       "country":"at",
       "language":"de",
       "name":"Austria"
    },
    { "pim" : false,
       "country":"by",
       "language":"ru",
       "name":"Belarus"
    },
    { "pim" : false,
       "country":"by",
       "language":"en",
       "name":"Belarus (English)"
    },
    { "pim" : true,
       "country":"be",
       "language":"fr",
       "name":"Belgium"
    },
    { "pim" : true,
       "country":"be",
       "language":"nl",
       "name":"Belgium (Dutch)"
    },
    { "pim" : false,
       "country":"ba",
       "language":"bs",
       "name":"Bosnia-Herzegovina"
    },
    { "pim" : true,
       "country":"bg",
       "language":"bg",
       "name":"Bulgaria"
    },
    { "pim" : true,
       "country":"hr",
       "language":"hr",
       "name":"Croatia"
    },
    { "pim" : false,
       "country":"cy",
       "language":"en",
       "name":"Cyprus"
    },
    { "pim" : true,
       "country":"cz",
       "language":"cs",
       "name":"Czech Republic"
    },
    { "pim" : true,
       "country":"dk",
       "language":"da",
       "name":"Denmark"
    },
    { "pim" : true,
        "country":"dk",
        "language":"en",
        "name":"Denmark"
     },
    { "pim" : true,
       "country":"ee",
       "language":"et",
       "name":"Estonia"
    },
    { "pim" : true,
        "country":"ee",
        "language":"en",
        "name":"Estonia"
     },
    { "pim" : true,
       "country":"fi",
       "language":"fi",
       "name":"Finland"
    },
    { "pim" : true,
        "country":"fi",
        "language":"en",
        "name":"Finland"
     },
    { "pim" : true,
       "country":"fr",
       "language":"fr",
       "name":"France"
    },
    { "pim" : false,
       "country":"ge",
       "language":"ka",
       "name":"Georgia"
    },
    { "pim" : false,
       "country":"ge",
       "language":"en",
       "name":"Georgia (English)"
    },
    { "pim" : true,
       "country":"de",
       "language":"de",
       "name":"Germany"
    },
    { "pim" : true,
       "country":"gr",
       "language":"el",
       "name":"Greece"
    },
    { "pim" : true,
        "country":"gr",
        "language":"en",
        "name":"Greece"
     },
    { "pim" : true,
       "country":"hu",
       "language":"hu",
       "name":"Hungary"
    },
    { "pim" : true,
       "country":"ie",
       "language":"en",
       "name":"Ireland"
    },
    { "pim" : true,
       "country":"it",
       "language":"it",
       "name":"Italy"
    },
    { "pim" : false,
       "country":"ks",
       "language":"sq",
       "name":"Kosovo"
    },
    { "pim" : true,
       "country":"lv",
       "language":"lv",
       "name":"Latvia"
    },
    { "pim" : true,
        "country":"lv",
        "language":"en",
        "name":"Latvia"
     },
    { "pim" : true,
       "country":"lt",
       "language":"lt",
       "name":"Lithuania"
    },
    { "pim" : true,
        "country":"lt",
        "language":"en",
        "name":"Lithuania"
     },
    { "pim" : false,
       "country":"mt",
       "language":"en",
       "name":"Malta"
    },
    { "pim" : false,
       "country":"md",
       "language":"ro",
       "name":"Moldavia"
    },
    { "pim" : false,
       "country":"me",
       "language":"me",
       "name":"Montenegro"
    },
    { "pim" : true,
       "country":"nl",
       "language":"nl",
       "name":"Netherlands"
    },
    { "pim" : true,
       "country":"mk",
       "language":"mk",
       "name":"North Macedonia"
    },
    { "pim" : true,
        "country":"mk",
        "language":"en",
        "name":"North Macedonia"
     },
    { "pim" : true,
       "country":"no",
       "language":"no",
       "name":"Norway"
    },
    { "pim" : true,
        "country":"no",
        "language":"en",
        "name":"Norway"
     },
    { "pim" : true,
       "country":"pl",
       "language":"pl",
       "name":"Poland"
    },
    { "pim" : true,
       "country":"pt",
       "language":"pt",
       "name":"Portugal"
    },
    { "pim" : true,
       "country":"ro",
       "language":"ro",
       "name":"Romania"
    },
    { "pim" : true,
        "country":"ro",
        "language":"en",
        "name":"Romania"
     },
    { "pim" : true,
       "country":"ru",
       "language":"ru",
       "name":"Russia"
    },
    { "pim" : true,
       "country":"rs",
       "language":"sr",
       "name":"Serbia"
    },
    { "pim" : true,
        "country":"rs",
        "language":"en",
        "name":"Serbia"
     },
    { "pim" : true,
       "country":"sk",
       "language":"sk",
       "name":"Slovakia"
    },
    { "pim" : true,
        "country":"si",
        "language":"en",
        "name":"Slovenia"
     },
    { "pim" : true,
       "country":"si",
       "language":"sl",
       "name":"Slovenia"
    },
    { "pim" : true,
       "country":"es",
       "language":"es",
       "name":"Spain"
    },
    { "pim" : true,
       "country":"se",
       "language":"sv",
       "name":"Sweden"
    },
    { "pim" : true,
       "country":"ch",
       "language":"fr",
       "name":"Switzerland (French)"
    },
    { "pim" : true,
       "country":"ch",
       "language":"de",
       "name":"Switzerland (German)"
    },
    { "pim" : true,
        "country":"ch",
        "language":"it",
        "name":"Switzerland (Italian)"
     },
    { "pim" : true,
       "country":"tr",
       "language":"tr",
       "name":"Turkey"
    },
    { "pim" : true,
       "country":"ua",
       "language":"ru",
       "name":"Ukraine (Russian)"
    },
    { "pim" : true,
       "country":"ua",
       "language":"uk",
       "name":"Ukraine (Ukrainian)"
    },
    { "pim" : false,
       "country":"uk",
       "language":"en",
       "name":"United Kingdom"
    },
    { "pim" : true,
       "country":"ar",
       "language":"es",
       "name":"Argentina Uruguay Paraguay"
    },
    { "pim" : false,
       "country":"pe",
       "language":"es",
       "name":"Bolivia"
    },
    { "pim" : true,
       "country":"br",
       "language":"pt",
       "name":"Brazil"
    },
    { "pim" : true,
       "country":"cl",
       "language":"es",
       "name":"Chile"
    },
    { "pim" : true,
       "country":"co",
       "language":"es",
       "name":"Colombia"
    },
    { "pim" : false,
       "country":"co",
       "language":"es",
       "name":"Ecuador"
    },
    { "pim" : false,
       "country":"gf",
       "language":"fr",
       "name":"French Guiana"
    },
    { "pim" : true,
       "country":"pe",
       "language":"es",
       "name":"Peru"
    },
    { "pim" : false,
       "country":"co",
       "language":"es",
       "name":"Venezuela"
    },
    { "pim" : true,
       "country":"dz",
       "language":"fr",
       "name":"Algeria"
    },
    { "pim" : false,
       "country":"az",
       "language":"az",
       "name":"Azerbaijan (Azerbaijani)"
    },
    { "pim" : false,
       "country":"az",
       "language":"en",
       "name":"Azerbaijan (English)"
    },
    { "pim" : false,
       "country":"ae",
       "language":"en",
       "name":"Bahrain (English)"
    },
    { "pim" : false,
       "country":"bj",
       "language":"fr",
       "name":"Benin"
    },
    { "pim" : false,
       "country":"bw",
       "language":"en",
       "name":"Botswana"
    },
    { "pim" : false,
       "country":"bf",
       "language":"fr",
       "name":"Burkina Faso"
    },
    { "pim" : false,
       "country":"cm",
       "language":"fr",
       "name":"Cameroon"
    },
    { "pim" : false,
       "country":"cf",
       "language":"fr",
       "name":"Central African Republic"
    },
    { "pim" : false,
       "country":"td",
       "language":"fr",
       "name":"Chad"
    },
    { "pim" : false,
       "country":"mg",
       "language":"fr",
       "name":"Comoros"
    },
    { "pim" : false,
       "country":"cg",
       "language":"fr",
       "name":"Congo"
    },
    { "pim" : false,
       "country":"cd",
       "language":"fr",
       "name":"Dem. Rep. Congo (Zaire)"
    },
    { "pim" : true,
       "country":"eg",
       "language":"ar",
       "name":"Egypt and North East Africa (Arabic)"
    },
    { "pim" : true,
       "country":"eg",
       "language":"en",
       "name":"Egypt and North East Africa (English)"
    },
    { "pim" : false,
       "country":"gq",
       "language":"es",
       "name":"Equatorial Guinea"
    },
    { "pim" : false,
       "country":"africa",
       "language":"fr",
       "name":"Francophone Africa"
    },
    { "pim" : false,
       "country":"ga",
       "language":"fr",
       "name":"Gabon"
    },
    { "pim" : false,
       "country":"gm",
       "language":"fr",
       "name":"Gambia"
    },
    { "pim" : false,
       "country":"gh",
       "language":"en",
       "name":"Ghana"
    },
    { "pim" : false,
       "country":"gn",
       "language":"fr",
       "name":"Guinea"
    },
    { "pim" : false,
       "country":"iq",
       "language":"en",
       "name":"Iraq"
    },
    { "pim" : false,
       "country":"il",
       "language":"he",
       "name":"Israel"
    },
    { "pim" : true,
        "country":"il",
        "language":"en",
        "name":"Israel"
     },
    { "pim" : false,
       "country":"ci",
       "language":"fr",
       "name":"Ivory Coast"
    },
    { "pim" : false,
       "country":"jo",
       "language":"en",
       "name":"Jordan"
    },
    { "pim" : false,
       "country":"ke",
       "language":"en",
       "name":"Kenya"
    },
    { "pim" : false,
       "country":"ae",
       "language":"en",
       "name":"Kuwait"
    },
    { "pim" : false,
       "country":"lb",
       "language":"en",
       "name":"Lebanon"
    },
    { "pim" : false,
       "country":"ng",
       "language":"en",
       "name":"Liberia"
    },
    { "pim" : false,
       "country":"ly",
       "language":"en",
       "name":"Libya"
    },
    { "pim" : false,
       "country":"mg",
       "language":"fr",
       "name":"Madagascar"
    },
    { "pim" : false,
       "country":"mw",
       "language":"en",
       "name":"Malawi"
    },
    { "pim" : false,
       "country":"ml",
       "language":"fr",
       "name":"Mali"
    },
    { "pim" : false,
       "country":"mr",
       "language":"fr",
       "name":"Mauritania"
    },
    { "pim" : false,
       "country":"mu",
       "language":"fr",
       "name":"Mauritius"
    },
    { "pim" : false,
       "country":"re",
       "language":"fr",
       "name":"Mayotte"
    },
    { "pim" : true,
       "country":"ma",
       "language":"fr",
       "name":"Morocco"
    },
    { "pim" : false,
       "country":"mz",
       "language":"pt",
       "name":"Mozambique"
    },
    { "pim" : false,
       "country":"na",
       "language":"en",
       "name":"Namibia"
    },
    { "pim" : false,
       "country":"ne",
       "language":"fr",
       "name":"Niger"
    },
    { "pim" : true,
       "country":"ng",
       "language":"en",
       "name":"Nigeria"
    },
    { "pim" : false,
       "country":"ae",
       "language":"en",
       "name":"Oman"
    },
    { "pim" : false,
       "country":"ae",
       "language":"en",
       "name":"Pakistan"
    },
    { "pim" : false,
       "country":"ae",
       "language":"en",
       "name":"Qatar"
    },
    { "pim" : false,
       "country":"re",
       "language":"fr",
       "name":"Reunion"
    },
    { "pim" : true,
       "country":"sa",
       "language":"ar",
       "name":"Saudi Arabia (Arabic)"
    },
    { "pim" : true,
       "country":"sa",
       "language":"en",
       "name":"Saudi Arabia (English)"
    },
    { "pim" : false,
       "country":"sn",
       "language":"fr",
       "name":"Senegal"
    },
    { "pim" : false,
       "country":"mu",
       "language":"fr",
       "name":"Seychelles"
    },
    { "pim" : false,
       "country":"ng",
       "language":"en",
       "name":"Sierra Leone"
    },
    { "pim" : true,
       "country":"za",
       "language":"en",
       "name":"South Africa"
    },
    { "pim" : false,
       "country":"sz",
       "language":"en",
       "name":"Swaziland"
    },
    { "pim" : false,
       "country":"tz",
       "language":"en",
       "name":"Tanzania"
    },
    { "pim" : false,
       "country":"tg",
       "language":"fr",
       "name":"Togo"
    },
    { "pim" : true,
       "country":"tn",
       "language":"fr",
       "name":"Tunisia"
    },
    { "pim" : false,
       "country":"ug",
       "language":"en",
       "name":"Uganda"
    },
    { "pim" : true,
       "country":"ae",
       "language":"en",
       "name":"United Arab Emirates"
    },
    { "pim" : false,
       "country":"ye",
       "language":"en",
       "name":"Yemen"
    },
    { "pim" : false,
       "country":"zm",
       "language":"en",
       "name":"Zambia"
    },
    { "pim" : false,
       "country":"zw",
       "language":"en",
       "name":"Zimbabwe"
    },
    { "pim" : false,
       "country":"bs",
       "language":"en",
       "name":"Bahamas"
    },
    { "pim" : false,
       "country":"bb",
       "language":"en",
       "name":"Barbados"
    },
    { "pim" : true,
       "country":"ca",
       "language":"en",
       "name":"Canada"
    },
    { "pim" : true,
       "country":"ca",
       "language":"fr",
       "name":"Canada (French)"
    },
    { "pim" : true,
       "country":"cr",
       "language":"es",
       "name":"Costa Rica"
    },
    { "pim" : false,
       "country":"do",
       "language":"es",
       "name":"Dominican Republic"
    },
    { "pim" : false,
       "country":"sv",
       "language":"es",
       "name":"El Salvador"
    },
    { "pim" : false,
       "country":"gp",
       "language":"fr",
       "name":"Guadeloupe"
    },
    { "pim" : false,
       "country":"gt",
       "language":"es",
       "name":"Guatemala"
    },
    { "pim" : false,
       "country":"ht",
       "language":"fr",
       "name":"Haiti"
    },
    { "pim" : false,
       "country":"hn",
       "language":"es",
       "name":"Honduras"
    },
    { "pim" : false,
       "country":"jm",
       "language":"en",
       "name":"Jamaica"
    },
    { "pim" : false,
       "country":"mq",
       "language":"fr",
       "name":"Martinique"
    },
    { "pim" : true,
       "country":"mx",
       "language":"es",
       "name":"Mexico"
    },
    { "pim" : false,
       "country":"ni",
       "language":"es",
       "name":"Nicaragua"
    },
    { "pim" : false,
       "country":"pa",
       "language":"es",
       "name":"Panama"
    },
    { "pim" : false,
       "country":"gp",
       "language":"fr",
       "name":"St. Barthelemy"
    },
    { "pim" : false,
       "country":"mq",
       "language":"fr",
       "name":"St. Lucia"
    },
    { "pim" : false,
       "country":"gp",
       "language":"fr",
       "name":"St. Martin"
    },
    { "pim" : false,
       "country":"mq",
       "language":"fr",
       "name":"St. Pierre & Miquelon"
    },
    { "pim" : false,
       "country":"tt",
       "language":"en",
       "name":"Trinidad And Tobago"
    },
    { "pim" : true,
       "country":"us",
       "language":"en",
       "name":"USA"
    }
 ];

 var sortScopeList = scopelist.sort(function (a, b) {
    return a.name.localeCompare(b.name)
});

sortScopeList.forEach((e)=>{
     if (e.pim){
        var first = document.createElement("option");
        first.value=e.language+"-"+e.country.toUpperCase();
        first.innerHTML=e.name+" ("+e.language+"-"+e.country.toUpperCase()+")";
        document.getElementById("scope-list").appendChild(first);
     }
 })

 console.log(sortScopeList);