const createError = require('http-errors');
const express = require('express');
const path = require('path');
const mustacheExpress = require('mustache-express');
const fileUpload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const { runRef } = require('./utils/main');
const { readReferencesFromFile } = require('./utils/utils');

var indexRouter = require('./routes/index');

var app = express();

app.set('views', `${__dirname}/views`);
app.set('view engine', 'mustache');
app.engine('mustache', mustacheExpress());
app.use(fileUpload({
  createParentPath: true
}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

app.post('/getref', function(req, res){
  runRef(req.body).then(result=> {
    res.json(result);
  });
})

app.post('/ref', function(req, res){
  runRef(req.body).then(result=> {
    console.log(result);
    res.render("ref", {result : result});
  });
})

app.post('/refs', function(req, res){
  try {
    if(!req.files) {
        res.send({
            status: false,
            message: 'No file uploaded'
        });
    } else {
        let srcList = req.body;
        console.log(req.body);
        let refsFile = req.files.searchedRefs; 
        refsFile.mv('./uploads/' + refsFile.name).then(()=>{
          let list = readReferencesFromFile('./uploads/' + refsFile.name);
          res.render("refs", { data: { listJson : JSON.stringify(list), list: list, srcList : JSON.stringify(srcList), srcs : srcList }});
        });
    }
  } catch (err) {
      res.status(500).send(err);
  }
})


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});

module.exports = app;
