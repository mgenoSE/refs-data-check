
const soap = require('soap');
const { readReferencesFromFile, writeResultsToXlsx } = require('./utils');
const { check } = require('./check');

/*async function run() {
  const bslClient = await soap.createClientAsync('./resources/bsl.wsdl');
  const docsClient = await soap.createClientAsync('./resources/docs.wsdl');

  const products = readReferencesFromFile('./references.xlsx');

  console.log(`Found ${products.length} references.`);

  // run in sequence to prevent overloading BSL
  const results = [];
  await products.reduce((p, ref) => {
    return p.then(() => {
      return check(bslClient, docsClient, ref).then(result => {
        results.push(result);

        return true;
      });
    });
  }, Promise.resolve());

  console.log(results);

  writeResultsToXlsx(results, './results.xlsx');

}*/

async function runRef(search){
  const bslClient = await soap.createClientAsync('./resources/bsl.wsdl');
  const docsClient = await soap.createClientAsync('./resources/docs.wsdl');

  // run in sequence to prevent overloading BSL
  const refwithLocale = {
    reference : search.searchedRef,
    locale : search.searchedLocale,
    inputBsl : search.inputBsl ,
    inputWebsite : search.inputWebsite,
    inputGo2se : search.inputGo2se ,
    inputSr : search.inputSr,
    inputDsl : search.inputDsl
  }
  console.log(search);
  return await check(bslClient, docsClient, refwithLocale).then(result => {
    //console.log(result);
        return result;
      });
}

exports.runRef = runRef;