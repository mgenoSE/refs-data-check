const got = require('got');
const puppeteer = require('puppeteer');

async function check(bslClient, docsClient, search) {
  console.log(`Checking reference ${search.reference}...`);
  let result = {
    reference: search.reference,
    locale: search.locale,
    searchInput : search,
    product : null,
    bslStatus : null,
    hasGo2SE : null,
    hasSafeRepo : null,
    dslStatus : null,
    availableInWebsite : null
  };

  //BSL
  if(search.inputBsl != undefined){
    console.log("checking BSL");
    const product = await getProduct(bslClient, search);
    if (!product) {
      return result;
    }
    const hasDescriptionBSL = await checkDescription(product);
    const hasImageBSL = await checkImage(docsClient, product, search);
    const documentsBSL = await getDocuments(docsClient, product, search);
    result.product = product;
    result.bslStatus = {};
    result.bslStatus.hasImage = hasImageBSL;
    result.bslStatus.hasDescription = hasDescriptionBSL;
    result.bslStatus.hasDocuments = documentsBSL;
    

    if(search.inputGo2se != undefined){
      console.log("checking Go2SE");
      const hasGo2SE = await checkGo2SELink(search);
      result.hasGo2SE = hasGo2SE;
    }
  
    if(search.inputSr != undefined){
      console.log("checking Safe Repository");
      const hasSafeRepo = await checkSafeRepoLink(search);
      result.hasSafeRepo = hasSafeRepo;
    }
  }
  
  if(search.inputWebsite != undefined){
    console.log("checking Website");
    const availableInWebsite = await getWebpage(search);
    result.availableInWebsite = availableInWebsite;
  } 

  if(search.inputDsl != undefined){
    console.log("checking DSL");
    const hasImageDSL = await checkDSLImage(search);
    const hasDescriptionDSL = await checkDSLDescription(search);
    result.dslStatus = {};
    result.dslStatus.hasImage = hasImageDSL;
    result.dslStatus.hasDescriptionDSL = hasDescriptionDSL;
    console.log("end checking DSL");
  }
 
  return result;
  
}

async function checkGo2SELink(referenceWithLocale) {

  const url = `https://www.go2se.com/ref=${referenceWithLocale.reference}/sn=00000`;

  //set Cookie for localisation
  const browser0 = await puppeteer.launch({ headless: true });
  const country = referenceWithLocale.locale.split('-')[1];
  page0 = await browser0.newPage()
  await page0.goto('https://www.go2se.com/create/cookie?cookieName=qrcode_regn&cookieValue=' + country);
  cookies = await page0.cookies();
  browser0.close();

  // Test page
  const browser = await puppeteer.launch({ headless: true, args: [`--lang=${referenceWithLocale.locale}`] });
  const page = await browser.newPage();
  await page.setCookie(...cookies);
  await page.goto(url);
  await page.waitForSelector('.product-info-panel', {
    visible: true,
  });
  let data;
  try {
    data = await page.evaluate(() => {
      const defaultImgUrl = "https://download.schneider-electric.com/files?p_Doc_Ref=&p_File_Type=rendition_288_png&default_image=DefaultProductImage.png";
      const description = document.querySelector('.product-detail tr:nth-child(3) .p-info').innerText;
      const image = (document.querySelector(".product-image img").src != defaultImgUrl);
      const characteristics = document.querySelector("#characteristicsLink");
      return {
        isWorking: true,
        hasDescription: description.trim().length > 0,
        hasImage: image,
        hasCharacteristics: !!characteristics
      }
    });
  } catch (error) {
    //console.log(error);
    data = {
      isWorking: false,
      hasDescription: "error",
      hasImage: "error",
      hasCharacteristics: "error"
    }
  }

  await browser.close();

  return data;
}

async function checkSafeRepoLink(referenceWithLocale) {

  const url = `https://saferepository.schneider-electric.com/product/ref=${referenceWithLocale.reference}/sn=00000`;

  const browser = await puppeteer.launch({ headless: true, args: [`--lang=${referenceWithLocale.locale}`] });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: 'load', timeout: 500000 });
  let data;
  try {
    await page.waitForSelector('#productView', {
      visible: true,
    });
    await page.waitFor(5000);
    data = await page.evaluate(() => {
      const description = document.querySelector('#proddescription').innerText;
      const image = document.querySelector("img.productPicture");
      const documents = document.querySelector("#DocumentsGeneric .panel-heading").innerText;
      return {
        isWorking: true,
        hasDescription: description.trim().length > 0,
        hasImage: !!image,
        hasDocuments: documents.trim().split(" ")[0]
      }
    });
  } catch (error) {
    data = {
      isWorking: false,
      hasDescription: "error",
      hasImage: "error",
      hasDocuments: "error"
    }
  }

  await browser.close();

  return data;

}

async function getProduct(client, referenceWithLocale) {
  const lang = referenceWithLocale.locale.split('-')[0];
  const country = referenceWithLocale.locale.split('-')[1];
  let countryScope = country;
  if (country == "GB" && lang == "en") {
    countryScope = "WW";
  }
  const productArgs = {
    scope: {
      brand: 'Schneider Electric',
      country: countryScope
    },
    locale: {
      isoCountry: country,
      isoLanguage: lang
    },
    commercialRef: referenceWithLocale.reference,
    scopeIndependant: false
  };
  try {
    const result = await client.getProductDetailByCommercialRefV002Async(
      productArgs
    );
    return result[0]['ProductBean'];
  } catch (e) {
    return null;
  }
}

async function checkDescription(product) {

  return !!product.shortDescription && product.shortDescription.length > 0;
}

async function checkImage(client, product, referenceWithLocale) {
  const lang = referenceWithLocale.locale.split('-')[0];
  const country = referenceWithLocale.locale.split('-')[1];
  let countryScope = country;
  if (country == "GB" && lang == "en") {
    countryScope = "WW";
  }
  if (!product.pictureDocumentOid) {
    return false;
  }

  const docArgs = {
    scope: {
      brand: 'Schneider Electric',
      country: countryScope
    },
    locale: {
      isoCountry: country,
      isoLanguage: lang
    },
    docOid: product.pictureDocumentOid,
    scopeIndependant: true
  };
  try{
    const documentResult = await client.getDocDetailsAsync(docArgs);
    const document = documentResult[0].return;
    const url = `https://download.schneider-electric.com/files?p_Doc_Ref=${document.reference}&p_File_Type=rendition_288_png`;
    try {
      const response = await got(url);
      return url;
    } catch (error) {
      return false;
    }
  }catch{
    return false;
  } 
}

async function getDocuments(client, product, referenceWithLocale) {
  if (!product.documentOids) {
    return [];
  }

  const lang = referenceWithLocale.locale.split('-')[0];
  const country = referenceWithLocale.locale.split('-')[1];
  let countryScope = country;
  if (country == "GB" && lang == "en") {
    countryScope = "WW";
  }
  const docOids = product.documentOids;

  const args = {
    scope: {
      brand: 'Schneider Electric',
      country: countryScope
    },
    locale: {
      isoCountry: country,
      isoLanguage: lang
    },
    docOid: null,
    scopeIndependant: false
  };
  let promises = docOids.oid.map(doc => {
    args.docOid = doc;

    return client.getDocDetailsAsync(args);
  });

  const responses = await Promise.all(promises);

  return responses.map(response => {
    const document = response[0].return;

    return {
      reference: document.reference,
      title: document.title,
      url: `https://download.schneider-electric.com/files?p_Doc_Ref=${document.reference}`
    };
  });
}

async function checkDSLImage(search){
  var imgPath = 'https://selectconfig-data.schneider-electric.com/products/image/'+search.reference;
  try {
    const response = await got(imgPath);
    return imgPath;
  } catch (error) {
    return false;
  }
}

async function checkDSLDescription(search){
  var descPath = 'https://selectconfig-data.schneider-electric.com/products/'+search.reference+'/description/locale/en-GB';
  try {
    const response = await got(descPath);
    let description = JSON.parse(response.body).catalogue_product.long_desc.translation["en-GB"];
    if (description != ""){
      return description;
    }
    return false;
  } catch (error) {
    return false;
  }

}

async function getWebpage(search){
  const lang = search.locale.split('-')[0];
  const country = search.locale.split('-')[1];
  let countryScope = country.toLowerCase();
  if (country == "GB" && lang == "en") {
    countryScope = "ww";
  }
  var webpageUrl = 'https://www.se.com/'+countryScope+'/'+lang+'/product/'+search.reference+'?t='+Date.now();
  //console.log(webpageUrl);
  let url ;
  try {
    const request = await got(webpageUrl,{    
        hooks: {
          beforeRedirect: [
            (options, response) => {
              console.log(options.url.href);
              url = options.url.href;
            }
          ]
        }
      }); 
    //console.log(url);
    if (url.includes(search.reference)){
      return url;
    }else{
      return false;
    }
    
  } catch (error) {
    console.log(error);
    return false;
  }
}

/*async function getWebpage(search){
  const lang = search.locale.split('-')[0];
  const country = search.locale.split('-')[1];
  let countryScope = country.toLowerCase();
  if (country == "GB" && lang == "en") {
    countryScope = "ww";
  }
  var webpageUrl = 'https://www.se.com/'+countryScope+'/'+lang+'/product/'+search.reference;
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page.goto(webpageUrl);
  let data =false;
  try {
    await page.waitFor(5000);
    console.log(page.url());
    if (page.url().includes(search.reference)){
      return webpageUrl;
    }
    await browser.close();
    return false;
  } catch (error) {
    console.log(error);
    await browser.close();
    return false;
  }
}*/


exports.check = check;
