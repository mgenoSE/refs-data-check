const fs = require('fs');
const xlsx = require('node-xlsx');

function readReferencesFromFile(file) {
  //const fileContent = fs.readFileSync(file, 'utf8');
 
  const fileContent = xlsx.parse(file);
  console.log(fileContent[0].data);
  const rawReferences = fileContent[0].data.map(row => {
    return { searchedRef: row[0].toString().trim(), searchedLocale: row[1].trim() }
  });
  const products = rawReferences
    .filter(row => {
      return row.searchedRef.length > 0;
    });

  return products;
}

function writeResultsToXlsx(results) {
  const data = results.map(result => {
    const documentsCell = {
      v: result.documents.map(doc => doc.title).join('\n'),
      s: {
        alignment: {
          wrapText: true
        }
      }
    };
    return [
      result.reference,
      result.locale,
      result.rangeId,
      result.hasDescription,
      result.hasImage,
      result.hasGo2SE.isWorking,
      result.hasGo2SE.hasImage,
      result.hasGo2SE.hasDescription,
      result.hasGo2SE.hasCharacteristics,
      result.hasSafeRepo.isWorking,
      result.hasSafeRepo.hasImage,
      result.hasSafeRepo.hasDescription,
      result.hasSafeRepo.hasDocuments,
      result.documents.length,
      documentsCell
    ];
  });

  console.log("Data OK");
  data.unshift([
    'Reference',
    'Locale',
    'Range Id',
    'Has description in BSL',
    'Has image in BSL',
    'Go2SE is working',
    'Go2SE has image',
    'Go2SE has description',
    'Go2SE has characteristics',
    'Safe Repository is working',
    'Safe Repository has image',
    'Safe Repository has description',
    'Safe Repository has documents',
    'Number of Documents',
    "Documents"
  ]);

  const options = {
    '!cols': [
      {
        wch: 20
      },
      {
        wch: 10
      },
      {
        wch: 10
      },
      {
        wch: 15
      },
      {
        wch: 15
      },
      {
        wch: 15
      },
      {
        wch: 20
      },
      {
        wch: 15
      },
      {
        wch: 20
      },
      {
        wch: 20
      },
      {
        wch: 15
      },
      ,
      {
        wch: 20
      },
      {
        wch: 15
      },
      {
        wch: 80
      }
    ]
  };
  console.log("create buffer");
  const buffer = xlsx.build([{ name: 'references', data: data }], options);

  console.log("write in file");
  // write to output file
  try {
    fs.writeFileSync('./results.xlsx', buffer, 'utf-8');
  } catch (e) {
    console.log(e);
  }

}

exports.readReferencesFromFile = readReferencesFromFile;
exports.writeResultsToXlsx = writeResultsToXlsx;
